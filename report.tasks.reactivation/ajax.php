<?php
die();
use Bitrix\Main\Config\Option;

define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC','Y');
define('NO_AGENT_CHECK', true);
define('STOP_STATISTICS', true);
define('PUBLIC_AJAX_MODE', true);
define('DisableEventsCheck', true);
require_once($_SERVER[ 'DOCUMENT_ROOT' ]. '/bitrix/modules/main/include/prolog_before.php' );

\Bitrix\Main\Loader::includeModule('main');
\Bitrix\Main\Loader::includeModule('crm');
\Bitrix\Main\Loader::includeModule('tasks');

$GLOBALS['APPLICATION']->RestartBuffer();
Header('Content-Type: application/json');
echo json_encode( $return );
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
die();