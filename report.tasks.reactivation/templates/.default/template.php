<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

//CJSCore::Init("sidepanel");
//CJSCore::Init("CJSTask");
//CJSCore::Init("tasks_integration_socialnetwork");
//$GLOBALS['APPLICATION']->SetAdditionalCSS("/bitrix/js/tasks/css/tasks.css");

\Bitrix\Main\Loader::includeModule('ui');
\Bitrix\Main\UI\Extension::load('ui.icons');

$name = 'testX';

$APPLICATION->SetTitle('Отчет по реактивации');

$linkButton = new \Bitrix\UI\Buttons\Button([
    //'link' => '/',
    'text' => 'export XLS',
]);

\Bitrix\UI\Toolbar\Facade\Toolbar::addButton($linkButton);

\Bitrix\UI\Toolbar\Facade\Toolbar::addFilter([
    'FILTER_ID' => $name,
    'GRID_ID' => $name,
    'FILTER' => [
        ['id' => 'DATE', 'name' => 'Дата', 'type' => 'date', 'default' => true],
        [
            'id' => 'USER_ID',
            'name' => 'Ответственный',
            'type' => 'dest_selector',
            'default' => true,
            'params' => [

                'useNewCallback' => 'Y',
                //'lazyLoad' => 'Y',
                //'context' => $propName,
                'contextCode' => 'U',
                'enableUsers' => 'Y',
                //'userSearchArea' => 'I', // E
                'enableSonetgroups' => 'N',
                'enableDepartments' => 'Y',
                'allowAddSocNetGroup' => 'N',
                'departmentSelectDisable' => 'N',
                'showVacations' => 'Y',
                'dublicate' => 'N',
                'enableAll' => ' N',
                'departmentFlatEnable' => 'Y',
                'useSearch' => 'Y',
                'multiple' => 'Y',
                'enableEmpty' => 'N'
            ]
        ],
        [
            'id' => 'CLIENT_ID',
            'name' => 'Клиент',
            'type' => 'dest_selector',
            'default' => true,
            'params' => [

                //'lazyLoad' => 'Y',
                //'contextCode' => 'CRM',
                'enableUsers' => 'N',
                'enableSonetgroups' => 'N',
                'enableDepartments' => 'N',
                'allowAddSocNetGroup' => 'N',
                'departmentSelectDisable' => 'Y',
                'showVacations' => 'Y',
                'dublicate' => 'N',
                //'userSearchArea' => 'I', // E
                'enableAll' => 'N',
                'departmentFlatEnable' => 'Y',
                'useSearch' => 'Y',
                'multiple' => 'Y',
                'enableEmpty' => 'N',

                'enableCrm' => 'Y',
                'returnItemUrl' => 'Y',

                'enableCrmLeads' => 'Y',
                'enableCrmContacts' => 'Y',
                'enableCrmCompanies' => 'Y',
                'enableCrmDeals' => 'Y',
                'enableCrmProducts' => 'N',
                'enableCrmQuotes' => 'N',
                'enableCrmOrders' => 'Y',

                'addTabCrmLeads' => 'Y',
                'addTabCrmContacts' => 'Y',
                'addTabCrmCompanies' => 'Y',
                'addTabCrmDeals' => 'Y',
                'addTabCrmProducts' => 'N',
                'addTabCrmQuotes' => 'N',
                'addTabCrmOrders' => 'Y',
            ]
        ]
    ],
    'ENABLE_LIVE_SEARCH' => true,
    'ENABLE_LABEL' => true,
    'THEME' => 'BORDER'
]);



$filterOption = new Bitrix\Main\UI\Filter\Options($name);
$filterData = $filterOption->getFilter(['DATE', 'USER_ID']);
$filter = [];

foreach ($filterData as $k => $v) {
    // Тут разбор массива $filterData из формата, в котором его формирует main.ui.filter в формат, который подойдет для вашей выборки.
    // Обратите внимание на поле "FIND", скорее всего его вы и захотите засунуть в фильтр по NAME и еще паре полей
    $filter['NAME'] = $filterData['FIND'];
}

//echo '<pre>'; print_r( $filterOption ); echo '</pre>';
//
//echo '<pre>'; print_r( $filterData ); echo '</pre>';


//$list = [
//    [
//        'data'    => [ //Данные ячеек
//            "ID" => 1,
//            "NAME" => "Название 1",
//            "AMOUNT" => 1000,
//            "PAYER_NAME" => "Плательщик 1"
//        ],
//        'actions' => [ //Действия над ними
//            [
//                'text'    => 'Редактировать',
//                'onclick' => 'document.location.href="/accountant/reports/1/edit/"'
//            ],
//            [
//                'text'    => 'Удалить',
//                'onclick' => 'document.location.href="/accountant/reports/1/delete/"'
//            ]
//
//        ],
//    ]
//];


$grid_options = new Bitrix\Main\Grid\Options($name);
$sort = $grid_options->GetSorting(['sort' => ['ID' => 'DESC'], 'vars' => ['by' => 'by', 'order' => 'order']]);
$nav_params = $grid_options->GetNavParams();

$nav = new Bitrix\Main\UI\PageNavigation($name);
$nav->allowAllRecords(true)
    ->setPageSize($nav_params['nPageSize'])
    ->initFromUri();

// Кнопка удалить
$onchange = new \Bitrix\Main\Grid\Panel\Snippet\Onchange();
$onchange->addAction(
    [
        'ACTION' =>  \Bitrix\Main\Grid\Panel\Actions::CALLBACK,
        'CONFIRM' => true,
        'CONFIRM_APPLY_BUTTON'  => 'Подтвердить',
        'DATA' => [
            ['JS' => 'Grid.removeSelected()']
        ]
    ]
);

//echo '<pre>'; print_r( $arResult['TASKS'] ); echo '</pre>';

//echo '<pre>'; print_r( $_REQUEST ); echo '</pre>';


    echo '<pre>'; print_r( $arResult['TASKS'] ); echo '</pre>';

$APPLICATION->IncludeComponent(
    'bitrix:main.ui.grid',
    '',
    [
        'GRID_ID' => $name,
        'HEADERS' => $arResult['COLUMNS'], // COLUMNS
        'ROWS' => $arResult['ROWS'],
        'SHOW_ROW_CHECKBOXES' => true,
        'NAV_OBJECT' => $nav,
        'AJAX_MODE' => 'Y',
        'AJAX_ID' => \CAjax::getComponentID('bitrix:main.ui.grid', '.default', ''),
        'PAGE_SIZES' => [
            ['NAME' => '5', 'VALUE' => '5'],
            ['NAME' => '10', 'VALUE' => '10'],
            ['NAME' => '20', 'VALUE' => '20'],
            ['NAME' => '50', 'VALUE' => '50'],
        ],
        'AJAX_OPTION_JUMP'          => 'N',
        'SHOW_CHECK_ALL_CHECKBOXES' => true,
        'SHOW_ROW_ACTIONS_MENU'     => true,
        'SHOW_GRID_SETTINGS_MENU'   => true,
        'SHOW_NAVIGATION_PANEL'     => true,
        'SHOW_PAGINATION'           => true,
        'SHOW_SELECTED_COUNTER'     => true,
        'SHOW_TOTAL_COUNTER'        => true,
        'SHOW_PAGESIZE'             => true,
        'SHOW_ACTION_PANEL'         => true,
        'ACTION_PANEL'              => [
            'GROUPS' => [
                'TYPE' => [
                    'ITEMS' => [
                        [
                            'ID'    => 'set-type',
                            'TYPE'  => 'DROPDOWN',
                            'ITEMS' => [
                                ['VALUE' => '', 'NAME' => '- Выбрать -'],
                                ['VALUE' => 'plus', 'NAME' => 'Поступление'],
                                ['VALUE' => 'minus', 'NAME' => 'Списание']
                            ]
                        ],
                        [
                            'ID'       => 'edit',
                            'TYPE'     => 'BUTTON',
                            'TEXT'        => 'Редактировать',
                            'CLASS'        => 'icon edit',
                            'ONCHANGE' => ''
                        ],
                        [
                            'ID'       => 'delete',
                            'TYPE'     => 'BUTTON',
                            'TEXT'     => 'Удалить',
                            'CLASS'    => 'icon remove',
                            'ONCHANGE' => $onchange->toArray()
                        ],
                    ],
                ]
            ],
        ],
        'ALLOW_COLUMNS_SORT'        => true,
        'ALLOW_COLUMNS_RESIZE'      => true,
        'ALLOW_HORIZONTAL_SCROLL'   => true,
        'ALLOW_SORT'                => true,
        'ALLOW_PIN_HEADER'          => true,
        'AJAX_OPTION_HISTORY'       => 'Y'
    ]
);