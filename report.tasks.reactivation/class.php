<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
Loc::loadMessages($_SERVER[ 'DOCUMENT_ROOT' ] . '/bitrix/modules/tasks/classes/general/task.php');

class ReportTasksReactivationComponent extends CBitrixComponent
{
    public $columns = [];
    public $mandatoryFields = [
        'ID',
        //'RESPONSIBLE.ID',
        //'RESPONSIBLE.NAME',
        //'RESPONSIBLE.LAST_NAME',
        'UF_CALL_LINK',
        'UF_CITY',
        'UF_DIALOG_LINK',
        'UF_CRM_TASK',
        'UF_AUTO_576562789381'
    ];

    /**
     * @return array|mixed
     * @throws TasksException
     * @throws \Bitrix\Main\SystemException
     */
    public function executeComponent()
    {
        $this->arResult = [];

        $this->cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $this->cacheTtl = 0 ;// (int)$this->arParams['TTL'] ? : 36000;

        $this->propertyUserType = \CIBlockProperty::GetUserType();

        $this->taskFieldsInfo = \CTasks::getFieldsInfo();

        \Bitrix\Main\Diag\Debug::startTimeLabel($this->getName() );

        // собственные методы
        $this->arResult['ROWS'] = $this->getTaskList();

        // подсчет времени
        \Bitrix\Main\Diag\Debug::endTimeLabel($this->getName());
        $this->arResult['init'] = round(\Bitrix\Main\Diag\Debug::getTimeLabels()[$this->getName()]['time'], 2, PHP_ROUND_HALF_UP);

        $this->includeComponentTemplate();

        return $this->arResult;
    }

    /**
     * Вывод исходного списка задач
     * @return array
     * @throws TasksException
     */
    private function getTaskList() : array
    {
        // "PARAMS" => [ 'NAV_PARAMS' => [ "nPageSize" => $nPageSize, 'iNumPage' => $step ] ],

        if ($this->cacheTtl > 0 && $this->cache->read(60*10, __METOD__ ))
        {
            return $this->cache->get(__METOD__);
        }

        $result = [];

        $this->getColumns();

        $db = \CTasks::GetList(
            ['DEADLINE' => 'DESC'],
            [
                'GROUP_ID' => 30,
                'STATUS' => [4,5]
            ],
            $this->columns,
            ['nPageTop' => 15],
            []
        );

        while ( $list = $db->Fetch() )
        {
            $this->getRenderFieldType($list);

            $result[ $list['ID'] ] = ['data' => $list, 'action' => false];
        }

        $this->cache->set(__METOD__, $result);

        return $result;
    }


    /**
     * Получение списка полей
     * @return array
     */
    public function getColumns() : array
    {
        $this->columns = array_merge(
            $this->getTaskFields(),
            $this->mandatoryFields
        );

        $result = [];

        foreach ( $this->columns as $column )
        {
            if ( $this->getFieldName($column) === $column && !in_array($column, $this->mandatoryFields, true) )
            {
                continue;
            }

            $result[ $column ] = [

                'id' =>  $column,
                'name' =>  $this->getFieldName($column),
                'sort' =>  $column,
                'default' => true
                //'column' => $this->getColumnData( $column )
            ];
        }

        $this->arResult['COLUMNS'] = $result;

        return $result;
    }

    public function getColumnData( string $field )
    {
        /*$field['LIST_SECTIONS_URL'] = $arParams['LIST_SECTIONS_URL'];
        $field['LIST_URL'] = $arParams['LIST_URL'];
        $field['SOCNET_GROUP_ID'] = $arParams['SOCNET_GROUP_ID'];
        $field['LIST_ELEMENT_URL'] = $arParams['~LIST_ELEMENT_URL'];
        $field['LIST_FILE_URL'] = $arParams['~LIST_FILE_URL'];
        $field['IBLOCK_ID'] = $arResult['IBLOCK_ID'];
        $field['SECTION_ID'] = $iblockSectionId;
        $field['ELEMENT_ID'] = $data['ID'];
        $field['FIELD_ID'] = $fieldId;
        $valueKey = (substr($fieldId, 0, 9) == 'PROPERTY_') ? $fieldId : '~'.$fieldId;
        $field['VALUE'] = $listValues[$data['ID']][$valueKey];*/
        //return \Bitrix\Lists\Field::renderField($option);
    }

    public function GetFolder()
    {
        return __DIR__;
    }

    /**
     * Рендер данных
     * @param array $list
     * @return array
     */
    public function getRenderFieldType( array &$list ) : array
    {
        global $USER, $APPLICATION;

        // подключение готовых функций битрикса для обработки полей
        require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/components/bitrix/tasks.task.list/templates/.default/result_modifier.php';

        // последовательная обработка значений определнных полей
        foreach ( $list as $fieldName => $fieldData )
        {
            $option = [
                'FIELD_ID'  => $fieldName,
                'NAME'      => $this->getFieldName($fieldName),
                'VALUE'     => $fieldData,
                'MULTIPLE'  => is_array($fieldData) ? 'Y' : 'N',
                'IS_REQUIRED' => 'N',
                'EDITABLE'  => false
            ];

            switch ( $fieldName )
            {
                case 'ID' :
                //case 'PARENT_ID':
                //$option['TYPE'] = 'ID';

                    break;

                case 'TITLE' :

                    $option['VALUE'] = '<a href="/company/personal/user/'.$USER->GetID().'/tasks/task/view/'.$list['ID'].'/" class="task-title">'.$option['VALUE'].'</a>';

                    break;

                case 'DESCRIPTION' :

                    //$option['VALUE'] = '<a href="/company/personal/user/'.$USER->GetID().'/tasks/task/view/'.$list['ID'].'/" class="task-title">открыть задачу</a>';

                    //$option['TYPE'] = 'S:HTML';
                    //$option['PROPERTY_USER_TYPE'] = $this->getPropertyUserType('HTML');
                    //$list['DEFAULT_VALUE']['TYPE'] = 'HTML';
                    //$list['DEFAULT_VALUE']['TEXT'] = $fieldData;

                    break;

                case 'CLOSED_BY':
                case 'STATUS_CHANGED_BY':
                case 'CHANGED_BY':
                case 'CREATED_BY':
                case 'RESPONSIBLE_ID' :
                    $option['TYPE'] = 'CREATED_BY';
                    break;

                case stripos($fieldName, 'DATE') :

                    $option['PROPERTY_USER_TYPE'] = $this->getPropertyUserType('DateTime');

                    break;

                case 'UF_CRM_TASK' :
                case 'UF_CRM_TASK_LEAD' :
                case 'UF_CRM_TASK_CONTACT' :
                case 'UF_CRM_TASK_COMPANY' :
                case 'UF_CRM_TASK_DEAL':
                case 'UF_CITY':
                case 'UF_CALL_LINK':
                case 'UF_DIALOG_LINK':
                case 'UF_AUTO_576562789381':

                    $option['TYPE'] = 'S:ECrm';
                    $option['PROPERTY_USER_TYPE'] = $this->getPropertyUserType($option['TYPE']);

                    break;
            }

            $list[ $fieldName ] = $option['VALUE'];

            if ( $option['PROPERTY_USER_TYPE'] )
            {
                $option['PROPERTY_TYPE'] =  $option['PROPERTY_USER_TYPE']['PROPERTY_TYPE'];
            }

            if( isset($option['TYPE']) )
            {
                $list[ $fieldName ] = \Bitrix\Lists\Field::renderField($option);
                if ( !$list[$fieldName] && $option['VALUE'] ) {
                    $list[ $fieldName ] = $option[ 'VALUE' ];
                }
            }
        }

        // обработка значений списков
        foreach( $this->taskFieldsInfo as $caseName => $value )
        {
            if ( $value['type'] === 'enum' && $value['values'] ) {
                $list[ $caseName ] = $this->taskFieldsInfo[ $caseName ][ 'values' ][ $list[ $caseName ] ];
            }
        }

        return $list;
    }

    /**
     * Получаем список всех стандартных полей для CTasks
     * @return array
     */
    public function getTaskFields() : array
    {
        $table = \Bitrix\Tasks\TaskTable::getTableName();

        return $this->getStandartFieldsName( $table );
    }

    /**
     * Метод получения списка наименований полей таблицы
     * @param string|null $table
     * @return array
     */
    public function getStandartFieldsName( string $table = null ) : array
    {
        if ($this->cacheTtl > 0 && $this->cache->read($this->cacheTtl, __METOD__) )
        {
            return $this->cache->get(__METOD__);
        }

        $result = [];
        global $DB;
        $strSql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME LIKE '".strtoupper($table)."'";
        $db = $DB->Query($strSql, true );
        while ( $list = $db->Fetch() )
        {
            $result[] = $list['COLUMN_NAME'];
        }

        $this->cache->set(__METOD__, $result);

        return $result;
    }

    /**
     * Получение человекоподобного названия свойства
     * @param string $id
     * @param string $key
     *
     * @return string
     */
    public function getFieldName( string $id, string $key = 'TASKS_FIELDS' ) : string
    {
        $result = \Bitrix\Main\Localization\Loc::getMessage($id);

        if ( !$result )
        {
            $result = \Bitrix\Main\Localization\Loc::getMessage($key . '_' . $id);
        }

        return $result ? : $id ;
    }

    /**
     * Получение свойств пользовательский полей Битрикс
     * @param string $type
     * @return array
     */
    public function getPropertyUserType( string $type ) : array
    {
        $split = ':';
        if ( strpos($type, $split) !== false ) {
            $type = explode($split, $type)[ 1 ];
        }

        if ( !$type ) {
            return [];
        }

        return $this->propertyUserType[$type];
    }
}