<?
$MESS["TASKS_EXCEL_ALLOW_TIME_TRACKING"] = "Śledzenie spędzonego czasu";
$MESS["TASKS_EXCEL_CHANGED_DATE"] = "Data modyfikacji";
$MESS["TASKS_EXCEL_CLOSED_DATE"] = "Data zakończenia";
$MESS["TASKS_EXCEL_CREATED_BY"] = "Stworzone przez";
$MESS["TASKS_EXCEL_CREATED_DATE"] = "Data utworzenia";
$MESS["TASKS_EXCEL_GROUP_NAME"] = "Projekt";
$MESS["TASKS_EXCEL_ID"] = "ID";
$MESS["TASKS_EXCEL_MARK"] = "Ocena";
$MESS["TASKS_EXCEL_ORIGINATOR_NAME"] = "Stworzone przez";
$MESS["TASKS_EXCEL_PRIORITY"] = "Priorytet";
$MESS["TASKS_EXCEL_RESPONSIBLE_ID"] = "Osoba odpowiedzialna";
$MESS["TASKS_EXCEL_RESPONSIBLE_NAME"] = "Osoba odpowiedzialna";
$MESS["TASKS_EXCEL_TIME_SPENT_IN_LOGS"] = "Czas spędzony";
$MESS["TASKS_EXCEL_UF_CRM_TASK"] = "CRM";
$MESS["TASKS_LIST_CRM_TYPE_CO"] = "Firma";
$MESS["TASKS_STATUS_1"] = "Nowe";
$MESS["TASKS_STATUS_2"] = "Oczekujące";
$MESS["TASKS_STATUS_3"] = "W toku";
$MESS["TASKS_STATUS_4"] = "Oczekujące potwierdzenia";
$MESS["TASKS_STATUS_5"] = "Zakończone";
?>