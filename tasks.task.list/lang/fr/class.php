<?
$MESS["TASKS_GROUP_ACTION_DAYS_NUM_INVALID_TEXT"] = "Ce champ ne peut contenir que des chiffres.";
$MESS["TASKS_GROUP_ACTION_DAYS_NUM_INVALID_TITLE"] = "Entrée invalide";
$MESS["TASKS_GROUP_ACTION_ERROR_MESSAGE"] = "\"#MESSAGE#\" dans ces tâches : #TASK_IDS#";
$MESS["TASKS_GROUP_ACTION_ERROR_TITLE"] = "Des erreurs sont survenues lors de l'actualisation groupée des tâches";
$MESS["TASKS_TL_ACCESS_TO_GROUP_DENIED"] = "Vous ne pouvez pas voir la liste de tâches de ce groupe.";
$MESS["TASKS_TL_FORUM_MODULE_NOT_INSTALLED"] = "Le module Forum n'est pas installé.";
$MESS["TASKS_TL_SOCIALNETWORK_MODULE_NOT_INSTALLED"] = "Le module Réseau social n'est pas installé.";
?>