<?
$MESS["TASKS_GROUP_ACTION_DAYS_NUM_INVALID_TEXT"] = "Este campo pode conter apenas números.";
$MESS["TASKS_GROUP_ACTION_DAYS_NUM_INVALID_TITLE"] = "Entrada inválida";
$MESS["TASKS_GROUP_ACTION_ERROR_MESSAGE"] = "\"#MESSAGE#\" nestas tarefas: #TASK_IDS#";
$MESS["TASKS_GROUP_ACTION_ERROR_TITLE"] = "Ocorreram erros ao atualizar um volume de tarefas";
$MESS["TASKS_TL_ACCESS_TO_GROUP_DENIED"] = "Você não pode ver a lista de tarefas para este grupo.";
$MESS["TASKS_TL_FORUM_MODULE_NOT_INSTALLED"] = "O módulo \"Fórum\" não está instalado.";
$MESS["TASKS_TL_SOCIALNETWORK_MODULE_NOT_INSTALLED"] = "O módulo \"Rede Social\" não está instalado.";
?>