<?
$MESS["TASKS_GROUP_ACTION_DAYS_NUM_INVALID_TEXT"] = "Este campo sólo puede contener números.";
$MESS["TASKS_GROUP_ACTION_DAYS_NUM_INVALID_TITLE"] = "Entrada Inválida";
$MESS["TASKS_GROUP_ACTION_ERROR_MESSAGE"] = "\"#MESSAGE#\" en estas tareas: #TASK_IDS#";
$MESS["TASKS_GROUP_ACTION_ERROR_TITLE"] = "Hubo errores durante la actualización masiva de tareas";
$MESS["TASKS_TL_ACCESS_TO_GROUP_DENIED"] = "No se puede ver la lista de tareas para este grupo.";
$MESS["TASKS_TL_FORUM_MODULE_NOT_INSTALLED"] = "El módulo \"Forum\" no está instalado.";
$MESS["TASKS_TL_SOCIALNETWORK_MODULE_NOT_INSTALLED"] = "El módulo \"Red social\" no está instalado.";
?>